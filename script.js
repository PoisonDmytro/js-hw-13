// 1. Опишіть своїми словами різницю між функціями `setTimeout()` і `setInterval(`)`.
// 2. Що станеться, якщо в функцію `setTimeout()` передати нульову затримку? Чи спрацює вона миттєво і чому?
// 3. Чому важливо не забувати викликати функцію `clearInterval()`, коли раніше створений цикл запуску вам вже не потрібен?

//setInterval - циклічний і враховує виконання код у інтервал часу, setTimeout - не циклічний і спочатку виконує код а потім інтервал

//Якщо код не складний для системи, то пофакту виконається миттєво

//щоб не забивати пам'ять

//------------
let counter = 1;
let imgList = document.querySelectorAll('.images-wrapper > img')
for(img of imgList){img.classList.add('hide');}
imgList[0].classList.add('active')
imgList[0].style.display ='block'
showImgLoop();
//---------------
function showImgLoop(){
    let showInterval = setInterval(()=>{
        let imgOld = document.querySelector('.active');
        let imgActive = imgList[counter];
        fadeOut(imgOld,500)
        fadeIn(imgActive,500,'block');
        counter++;
        if(counter == 4){
            counter = 0;
        }
    },3000)
    stopBtn.onclick = function(){
        clearInterval(showInterval);
    }
    unStopBtn.onclick = function(){
        showImgLoop();
    }
}

const fadeIn = (el, timeout, display) => {
    el.classList.add('active')
    el.style.opacity = 0;
    el.style.display = display || 'block';
    el.style.transition = `opacity ${timeout}ms`;
    setTimeout(() => {
        el.style.opacity = 1;
    }, 10);
};
const fadeOut = (el, timeout) => {
    el.classList.remove("active")
    el.style.opacity = 1;
    el.style.transition = `opacity ${timeout}ms`;
    el.style.opacity = 0;

    setTimeout(() => {
        el.style.display = 'none';
    }, timeout);
};
